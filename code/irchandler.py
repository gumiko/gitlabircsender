#! /usr/bin/env python
# -*- coding: utf8 -*-
#
#  gitlab irc sender
#  Copyright (C) 2016-2017  Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from code.configuration import Configuration
from code.core import Core

class IrcHandler():
    def printPushHeader(self, channel, project, branch, url, data, commits, commitsCount):
        if commitsCount > 1:
            if len(commits) < commitsCount:
                print("Wrong commits count. Got count {0} and real commits number {1}".format(commitsCount, len(commits)))
                commitsCount = len(commits)
            url = Configuration.gitlab_compare_path.format(
                homepage = url,
                commit1 = commits[0]["id"][:Configuration.gitlab_compare_commit_size] + "%5E",
                commit2 = commits[commitsCount - 1]["id"][:Configuration.gitlab_compare_commit_size]
            )
        else:
            url = Configuration.gitlab_commit_path.format(
                homepage = url,
                commit = commits[0]["id"][:Configuration.commit_size]
            )

        Core.messageHandler.add(channel, Configuration.irc_push_header_message.format(
            project = project,
            author = data["user_name"],
            count = commitsCount,
            branch = branch,
            url = url
        ))

    def printCommitAuthor(self, channel, project, data, commit, message):
        Core.messageHandler.add(channel, Configuration.irc_push_commit_message.format(
            project = project,
            author = commit["author"]["name"],
            shortid = commit["id"][:Configuration.commit_size],
            message = message
        ))

    def printCommitMessage(self, channel, messages):
        for msg in messages:
            Core.messageHandler.add(channel, Configuration.irc_push_commit_message_continue.format(
                message = msg
            ))

    def printPushTag(self, channel, project, data, commits, tag, url):
        if data["after"] != "0000000000000000000000000000000000000000":
            url = Configuration.gitlab_commit_path.format(
                homepage = url,
                commit = data["after"][:Configuration.commit_size]
            )
            Core.messageHandler.add(channel, Configuration.irc_push_add_tag_message.format(
                project = project,
                author = data["user_name"],
                shortid = data["after"][:Configuration.commit_size],
                url = url,
                tag = tag
            ))
        else:
            Core.messageHandler.add(channel, Configuration.irc_push_remove_tag_message.format(
                project = project,
                author = data["user_name"],
                tag = tag
            ))

    def printCreateBranch(self, channel, project, data, branch, url):
        after = data["after"]
        if after == "0000000000000000000000000000000000000000":
            Core.messageHandler.add(channel, Configuration.irc_push_delete_branch_message.format(
                project = project,
                author = data["user_name"],
                branch = branch
            ))
        else:
            url = Configuration.gitlab_commits_path.format(
                homepage = url,
                name = branch
            )
            Core.messageHandler.add(channel, Configuration.irc_push_create_branch_message.format(
                project = project,
                author = data["user_name"],
                branch = branch,
                shortid = data["after"][:Configuration.commit_size],
                url = url
            ))

    def printAddCommitNote(self, channel, project, data, commit, url, iid):
        url = Configuration.gitlab_commit_path.format(
            homepage = url,
            commit = "{0}#note_{1}".format(commit["id"][:Configuration.commit_size], iid),
        )
        Core.messageHandler.add(channel, Configuration.irc_push_add_note_message.format(
            project = project,
            author = data["user"]["name"],
            shortid = commit["id"][:Configuration.commit_size],
            url = url
        ))

    def printBuildFailed(self, channel, project, branch, data, commit, url, build_id, build_stage, build_name):
        url = Configuration.gitlab_build_path.format(
            homepage = url,
            build = build_id
        )
        Core.messageHandler.add(channel, Configuration.irc_build_failed_message.format(
            project = project,
            shortid = commit["sha"][:Configuration.commit_size],
            branch = branch,
            url = url,
            build = build_id,
            stage = build_stage,
            name = build_name
        ))

    def printBuildSuccess(self, channel, project, branch, data, commit, url, build_id):
        url = Configuration.gitlab_build_path.format(
            homepage = url,
            build = build_id
        )
        Core.messageHandler.add(channel, Configuration.irc_build_success_message.format(
            project = project,
            shortid = commit["sha"][:Configuration.commit_size],
            branch = branch,
            url = url,
            build = build_id
        ))

    def printStageFailed(self, channel, project, branch, data, commit, url, build_id):
        url = Configuration.gitlab_build_path.format(
            homepage = url,
            build = build_id
        )
        Core.messageHandler.add(channel, Configuration.irc_stage_failed_message.format(
            project = project,
            shortid = commit["sha"][:Configuration.commit_size],
            branch = branch,
            url = url,
            build = build_id
        ))

    def printPipelineFailed(self, channel, project, branch, data, commit, url, pipeline_id):
        url = Configuration.gitlab_pipeline_path.format(
            homepage = url,
            pipeline = pipeline_id
        )
        Core.messageHandler.add(channel, Configuration.irc_pipeline_failed_message.format(
            project = project,
            shortid = commit["id"][:Configuration.commit_size],
            branch = branch,
            url = url,
            pipeline = pipeline_id
        ))

    def printPipelineSuccess(self, channel, project, branch, data, commit, url, pipeline_id):
        url = Configuration.gitlab_pipeline_path.format(
            homepage = url,
            pipeline = pipeline_id
        )
        Core.messageHandler.add(channel, Configuration.irc_pipeline_success_message.format(
            project = project,
            shortid = commit["id"][:Configuration.commit_size],
            branch = branch,
            url = url,
            pipeline = pipeline_id
        ))

    def printMROpened(self, channel, project, branch, source_namespace, source_branch, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_merge_request_opened_message.format(
            project = project,
            branch = branch,
            url = url,
            source_namespace = source_namespace,
            source_branch = source_branch,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printMRClosed(self, channel, project, branch, source_namespace, source_branch, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_merge_request_closed_message.format(
            project = project,
            branch = branch,
            url = url,
            source_namespace = source_namespace,
            source_branch = source_branch,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printMRReopened(self, channel, project, branch, source_namespace, source_branch, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_merge_request_reopened_message.format(
            project = project,
            branch = branch,
            url = url,
            source_namespace = source_namespace,
            source_branch = source_branch,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printMRUpdated(self, channel, project, branch, source_namespace, source_branch, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_merge_request_updated_message.format(
            project = project,
            branch = branch,
            url = url,
            source_namespace = source_namespace,
            source_branch = source_branch,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printMRMerged(self, channel, project, branch, source_namespace, source_branch, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_merge_request_merged_message.format(
            project = project,
            branch = branch,
            url = url,
            source_namespace = source_namespace,
            source_branch = source_branch,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printAddMRNote(self, channel, project, branch, source_namespace, source_branch, data, url, iid):
        Core.messageHandler.add(channel, Configuration.irc_merge_request_add_note_message.format(
            project = project,
            branch = branch,
            url = url,
            source_namespace = source_namespace,
            source_branch = source_branch,
            author = data["user"]["name"],
            id = iid
        ))

    def printAddIssueNote(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_issue_add_note_message.format(
            project = project,
            url = url,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printAddSnippetNote(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_snippet_add_note_message.format(
            project = project,
            url = url,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printIssueOpened(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_issue_opened_message.format(
            project = project,
            url = url,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printIssueClosed(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_issue_closed_message.format(
            project = project,
            url = url,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printIssueReopened(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_issue_reopened_message.format(
            project = project,
            url = url,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printIssueUpdated(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_issue_updated_message.format(
            project = project,
            url = url,
            author = data["user"]["name"],
            title = title,
            id = iid
        ))

    def printCustomMessage(self, channel, message):
        Core.messageHandler.add(channel, message)

